# Components - Tailwind



## Getting started

This component was built using tailwind css. 
[Use the template at the bottom](#editing-this-readme)!


## Preview
- [Form Components](https://gitlab.com/rizahoemae/components/-/blob/main/form/1form.html)
![form.png](./preview-images/1form.png)
source design: [Vishnu Prasad's on Dribbble](https://dribbble.com/shots/16097562-Form-Elements/attachments/7947930?mode=media)
- [Hero images](https://gitlab.com/rizahoemae/components/-/blob/main/hero/1hero.html)
![hero.png](./preview-images/1hero.png)
source design: [Creative Tim's on Dribble](https://dribbble.com/shots/17013182-Winter-Specials/attachments/12094429?mode=media) and [Rachit Tank's on Unsplash](https://unsplash.com/photos/2cFZ_FB08UM)
- [Hero - Cyber](https://gitlab.com/rizahoemae/components/-/blob/main/hero/2hero.html)
![image.png](./image.png)
source design: [Blacklead Studio's on Dribble](https://dribbble.com/shots/17484265/attachments/12620322?mode=media), [Benyamin](https://unsplash.com/photos/jN8YF7gE2Vw), [Augustine](https://unsplash.com/photos/li0iC0rjvvg) and [Zac on Unsplash](https://unsplash.com/photos/OGB9mlMqsx8)
